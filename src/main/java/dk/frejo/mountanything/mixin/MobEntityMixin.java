package dk.frejo.mountanything.mixin;

import net.minecraft.entity.mob.MobEntity;
import net.minecraft.entity.player.PlayerEntity;
import net.minecraft.util.ActionResult;
import net.minecraft.util.Hand;
import org.spongepowered.asm.mixin.injection.At;
import org.spongepowered.asm.mixin.Mixin;
import org.spongepowered.asm.mixin.injection.Inject;
import org.spongepowered.asm.mixin.injection.callback.CallbackInfoReturnable;

@Mixin(MobEntity.class)
public class MobEntityMixin {
    @Inject(at = @At("HEAD"),
            method = "interactMob(Lnet/minecraft/entity/player/PlayerEntity;Lnet/minecraft/util/Hand;)Lnet/minecraft/util/ActionResult;",
            cancellable = true
    )
    private void interactMob(PlayerEntity player, Hand hand, CallbackInfoReturnable<ActionResult> cir) {
        MobEntity thisEntity = ((MobEntity)(Object)this);
        if (!thisEntity.hasPassengers() && !player.shouldCancelInteraction()) {
            if (!((MobEntity)(Object)this).world.isClient) {
                System.out.println("Starts riding entity");
                player.startRiding(thisEntity);
            }

            cir.setReturnValue(ActionResult.success(thisEntity.world.isClient));
        } else {
            cir.setReturnValue(ActionResult.PASS);
        }
    }
}
