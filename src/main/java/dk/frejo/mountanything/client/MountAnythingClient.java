package dk.frejo.mountanything.client;

import net.fabricmc.api.ClientModInitializer;
import net.fabricmc.api.EnvType;
import net.fabricmc.api.Environment;

@Environment(EnvType.CLIENT)
public class MountAnythingClient implements ClientModInitializer {
    @Override
    public void onInitializeClient() {

    }
}
